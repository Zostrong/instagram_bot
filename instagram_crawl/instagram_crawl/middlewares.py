# -*- coding: utf-8 -*-

# Define here the models for your spider middleware
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/spider-middleware.html
import time
from scrapy.http import HtmlResponse
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException


class ChromeMiddleware(object):
    def __init__(self):
        self.driver = webdriver.Chrome()
        self.first = True

        self.driver.get('https://instagram.com')
        continue_to_login = self.driver.find_element_by_xpath('//p[@class="_g9ean"]/a')
        continue_to_login.click()

        time.sleep(1)
        print('Username:')
        username = input()
        print('Password:')
        password = input()

        # p = getpass.getpass()
        form_username = self.driver.find_element_by_xpath('//input[@type="text"]')
        form_password = self.driver.find_element_by_xpath('//input[@type="password"]')
        submit_button = self.driver.find_element_by_xpath('//form[@class="_3jvtb"]//button')

        form_username.send_keys(username)
        form_password.send_keys(password)
        submit_button.click()

        time.sleep(1)

    def process_request(self, request, spider):
        self.driver.get(request.url)
        if self.first:
            picture_count = 200
            self.first = False
            for i in range(0, picture_count // 12 + 1):
                self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
                time.sleep(1)

        try:
            like = self.driver.find_element_by_xpath('//span[contains(@class, "coreSpriteHeartOpen")]')
            time.sleep(1)
            like.click()
            time.sleep(2)
        except NoSuchElementException:
            pass

        body = self.driver.page_source
        return HtmlResponse(self.driver.current_url, body=body, encoding='utf-8', request=request)

