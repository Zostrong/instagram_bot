import scrapy
from scrapy.exceptions import CloseSpider

from instagram_crawl.items import InstagramItem


class QuotesSpider(scrapy.Spider):
    name = 'instagram'
    person = ''
    hashtag = ''
    page_scanned = 0
    pictures = 0
    max_pictures = 0

    def start_requests(self):
        url = 'https://www.instagram.com/'

        self.hashtag = getattr(self, 'hashtag', '')
        self.person = getattr(self, 'person', '')
        try:
            self.max_pictures = int(getattr(self, 'max', 12))
        except Exception:
            raise CloseSpider(reason="Max value has to be a number")

        if self.person == '' and self.hashtag == '':
            raise CloseSpider('reason=Person or Hashtag has to be given')
        if self.person != '' and self.hashtag == '':
                url = url + self.person + '/'
        if self.hashtag != '' and self.person == '':
            url = url + 'explore/tags/' + self.hashtag + '/'

        yield scrapy.Request(url, self.parse)

    def parse(self, response):
        for href in response.css('div._cmdpi a'):
            if self.pictures < self.max_pictures:
                self.pictures = self.pictures + 1
                yield response.follow(href, self.parse_page)

    def parse_page(self, response):
        item = InstagramItem()
        yield item

